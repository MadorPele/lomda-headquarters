import { createRouter, createWebHashHistory } from 'vue-router'
import Intro from '../views/Intro.vue'
import Main from '../views/Main.vue'

const routes = [
  {
    path: '/',
    redirect: '/intro'
  }, 
  {
    path: '/intro',
    name: 'Intro',
    component: Intro
  },
  {
    path: '/main',
    name: 'Main',
    component: Main
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  mode: 'history',
  routes
})

export default router;
