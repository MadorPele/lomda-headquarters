import {
  createStore
} from 'vuex'

export default createStore({
  state: {
    currChapter: 0
  },
  getters: {
  },
  mutations: {
    changeChapter(state, num) {
      state.currChapter = num;
    }
  },
  actions: {},
  modules: {}
})